"""
This file contains all of the functions used within the notebooks.
Date: April 24, 2020
Author: Shawk Masboob

The function `uniform_sampling` was borrowed from Luis Polancocontreras,
a PhD candidate in the CMSE program at Michigan State University.
It was slightly tweaked to fit this project.
"""
from sklearn import ensemble
import kmapper as km
import numpy as np
import pandas as pd

def numpy_to_pandas(sklearn_data):
    """Converts scikit-learn numpy data into pandas dataframe

    Args:
        sklearn_data (array): name of dataframe

    Returns:
        data: pandas dataframe

    """
    data = pd.DataFrame(data=sklearn_data.data, columns=sklearn_data.feature_names)
    data['target'] = pd.Series(sklearn_data.target)
    return data

def lens_1d(x_array, proj='l2norm', random_num=1729, verbosity=0):
    """Creates a L^2-Norm for features. This lens highlights expected features in the data.

    Args:
        x_array (array): features of dataset </br>
        proj (string): projection type </br>
        random_num: random state </br>
        verbosity: verbosity </br>

    Returns:
        lens: Isolation Forest, L^2-Norm </br>
        mapper: projected features </br>

    """
    if not isinstance(x_array, np.ndarray):
        print("your input is not an array")
        return None, None
    if isinstance(x_array, np.ndarray) and len(x_array.shape) != 2:
        print('your input needs to be a 2d array')
        return None, None
    proj_type = ['sum', 'mean', 'median', 'max', 'min', 'std', 'dist_mean',
                 'l2norm', 'knn_distance_n']
    if proj not in proj_type:
        print("you may only use the following projections:", proj_type)
        return None, None
    # Create a custom 1-D lens with Isolation Forest
    model = ensemble.IsolationForest(random_state=random_num)
    model.fit(x_array)
    lens1 = model.decision_function(x_array).reshape((x_array.shape[0], 1))
    # Create another 1-D lens with L2-norm
    mapper = km.KeplerMapper(verbose=verbosity)
    lens2 = mapper.fit_transform(x_array, projection=proj)
    # Combine lenses pairwise to get a 2-D lens i.e. [Isolation Forest, L^2-Norm] lens
    lens = np.c_[lens1, lens2]
    return lens, mapper

def uniform_sampling(dist_matrix, n_sample):
    """Given a distance matrix retunrs an subsamplig that preserves the distribution
    of the original data set and the covering radious corresponding to
    the subsampled set.

    Args:
        dist_matrix (array): Distance matrix </br>
        n_sample (int): Size of subsample set </br>

    Returns:
        list_subsample (array): List of indices corresponding to the subsample set </br>
        distance_to_l: Covering radious for the subsample set </br>

    """
    if not isinstance(dist_matrix, np.ndarray):
        print("your input is not an array")
        return None, None
    if isinstance(dist_matrix, np.ndarray) and len(dist_matrix.shape) != 2:
        print('your input needs to be a 2d array')
        return None, None
    n_subsample = int(n_sample)
    if n_subsample <= 0:
        print("Sampling size should be a positive integer.")
        return None, None
    num_points = dist_matrix.shape[0]
    list_subsample = np.random.choice(num_points, n_subsample)
    dist_to_l = np.min(dist_matrix[list_subsample, :], axis=0)
    distance_to_l = np.max(dist_to_l)
    return list_subsample, distance_to_l
